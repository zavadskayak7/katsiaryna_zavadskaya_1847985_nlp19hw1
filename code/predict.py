from argparse import ArgumentParser
from keras.models import load_model
import utils
import train_model

def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_path", help="The path of the input file")
    parser.add_argument("output_path", help="The path of the output file")
    parser.add_argument("resources_path", help="The path of the resources needed to load your model")

    return parser.parse_args()


def predict(input_path, output_path, resources_path):
    """
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the BIES format.
    
    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.
    
    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.

    :param input_path: the path of the input file to predict.
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    
    arr_uni, arr_bi, arr, seq_len, org_seq_len = utils.uni_bi_dict(input_path,train_file=False,resources_path=resources_path) 
    train_model.run_model(arr_uni,arr_bi,arr,seq_len,org_seq_len,train=False, resources_path=resources_path, output_path=output_path)
    
    with open(input_path, encoding='utf-8', mode='r') as f:
        label_file = open(resources_path + '/labels.txt', 'w')
        lines = f.readlines()
        for line in lines:
            label_file.write(utils.bies_converter(line)) #filling up label file
        label_file.close()

if __name__ == '__main__':
    args = parse_args()
    predict(args.input_path, args.output_path, args.resources_path)
