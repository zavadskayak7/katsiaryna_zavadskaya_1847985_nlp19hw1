# -*- coding: utf-8 -*-
#imports
import numpy as np
import os
from nltk import ngrams
import json

def split_line(line, size, skip_tail=False):
  '''
  function to split a line of dataset into a list of lines with maximum len <
  padding_len, returns splitted list. the last element of the list is remainder
  = left over of division
  '''
  lst = []
  if size <= len(line):
      lst.extend([line[:size]])
      lst.extend(split_line(line[size:], size, skip_tail))
  elif not skip_tail and line:
      lst.extend([line])
  return lst



def write_padded_file(file_name = 'msr_training.utf8', padding_len = 70):
    ''' 
    creating a file with len of the lines less than padding_len

    '''
    
    with open(file_name, encoding='utf-8', mode='r') as f:
        proper_length_file = open('pad_len_' + file_name, 'w')
        for line in f.readlines():
            if len(line.strip()) <= padding_len: # if line is shorter, then leave it as it is
                proper_length_file.write(line)
            else:
                # if line is longer then split it in a list of lines,
                # where max len of list element is padding_len + '\n'
                splitted_line = split_line(line.strip(), padding_len) 
                for elem in splitted_line:
                    proper_length_file.write(elem+'\n') 
        proper_length_file.close()
      
      
def bies_converter(line):
    '''
    this function splits a string 'line' into a list of words,
    then each word is labeled according to BIES format:
        ● B: beginning - first character of each word=element of the list(line.split())
        ● I: inside - every character, which is surrounded by characters, labeled 'B'&'E'
        ● E: end - last character of each word
        ● S: single - word of length=1
        example:
            function's input: string 
            ('温带\u3000水域\u3000亦\u3000有\u3000其\u3000活动\u3000记录\u3000。')
            function's output: string of BIES labeles (ex: 'BEBESSSBEBES\n')
            '''
    bies_line = [] # list of BIES converted words
    for word in list(line.split()):
        if len(word) == 1:
            bies_line.append('S')
        elif len(word) == 2:
            bies_line.append('BE')
        elif len(word) > 2:
            bies_line.append('B' + 'I'*(len(word)-2) + 'E')
    label_line = ''.join(bies_line) + '\n' # BIES converted string
    return label_line
  
def write_input_label_files(file_name = 'msr_training.utf8'):
    # reading a dataset file with lines shorter than paddin_len
    # creating input file, deleting spaces
    # creating label file, using function bies_converter
    write_padded_file(file_name)
    with open('pad_len_' + file_name, encoding='utf-8', mode='r') as f:
        input_file = open('input_' + file_name + '.txt', 'w')
        label_file = open('label_' + file_name+ '.txt', 'w')
        lines = f.readlines()
        for line in lines:
            input_line = ''.join(line.split()) + '\n' # line without spaces for input file
            input_file.write(input_line)
            label_file.write(bies_converter(line)) #filling up label file
    input_file.close()
    label_file.close()

def get_len_sentences(file_name):
    with open(file_name, encoding='utf-8-sig', mode='r') as f:
        seq_len = []
        lines = f.readlines()
        for line in lines:
            input_line = ''.join(line.split())
            seq_len.append(len(input_line))
    return seq_len

def uni_bi_dict(file_name = 'msr_training.utf8', train_file=True, resources_path = '.'):
        
    n = 2 # the len of n-gram
    uni_grams_set = set() # set for saving all the unique unigrams from the dataset
    bigrams_set = set() # set for saving all the unique bigrams from the dataset
    # bigram is a sequence of two characters without space between them
    
# opening file
    print("Creating useful files ...") 
    write_input_label_files(file_name)
    print("Done")
    with open('input_' + file_name + '.txt', encoding='utf-8', mode='r') as f:
        lines = f.readlines()
        for line in lines:
            # uni_grams_set is a set of all the characters, used in dataset
            uni_grams_set = uni_grams_set.union(set(line.strip()))
        
            # bigrams - object that has inside tuples of unigrams: [('ch1','ch2'),('ch2','ch3'),...]
            bigrams = ngrams(line.strip(), n) # line.strip() is a list of single characters
            for ngram in bigrams:
                elem = ''.join(list(ngram)) # concatenate elements of tuple and save as a bigram
                bigrams_set.add(elem) # set of all unique bigrams from the dataset
          
        # creating dictionary with keys - dataset unique characters, values - integers
        if train_file:
            keys = ['<pad>', '<unk>'] + list(uni_grams_set) + list(bigrams_set)
            values = list(range(len(keys)))
            n_grams_dictionary = dict(zip(keys, values)) # dictionary of the dataset
            #saving dictionary into json file
            with open(resources_path + 'n_grams_dictionary_msr.json', 'w') as fp:
                json.dump(n_grams_dictionary, fp)
        else:
            with open(resources_path + 'n_grams_dictionary_msr.json', 'r') as fp:
                n_grams_dictionary = json.load(fp)
                
    print("Dictionaries created")
    # transforming label file into integers (ex: BIIIES --> 011123) Y_TRAIN
    # create a dict for bies mapping
    bies_dict = {'B':0, 'I':1, 'E':2, 'S':3}
    # create array for label file transforming using bies dict
    label_arr = list() # list to save array of integers for every line
    # opening label file
    with open('label_' + file_name + '.txt', encoding='utf-8', mode='r') as f:
        lines = f.readlines()
        for line in lines:
            # substituting every character with integer using bies_dict
            line_array = np.array([bies_dict.get(elem) for elem in list(line.strip())])
            label_arr.append(line_array)
            arr = np.array(label_arr, dtype=object) # final array of transformation label file into integers
      
      
    # transforming input file into integers
    # one transformation into unigrams array, another - into bigrams array
    # n = 2 - size of the ngrams = bigrams, was defined before
    unigram_arr = list() # list, which elements are arrays of integers for every unigram in line
    bigram_arr = list() # list, which elements are arrays of integers for every bigram in line
    # opening input file
    print("constructing bigrams unigrans")
    with open('input_' + file_name + '.txt', encoding='utf-8', mode='r') as f:
        lines = f.readlines()
        for line in lines:
            # UNIGRAMS
            # line_array_uni - array of integers for every single character in a line
            line_array_uni = []
            for elem in list(line.strip()):
                if elem in n_grams_dictionary.keys():
                    line_array_uni.append(n_grams_dictionary.get(elem))
                else:
                    line_array_uni.append(n_grams_dictionary.get('<unk>'))
            unigram_arr.append(np.array(line_array_uni)) # unigram_arr - list of arrays
            # final array of transformation input file unigrams into integers
            # BIGRAMS
            line_array_bi = [] # list of integers for every bigram in a line 
            bigrams = ngrams(line.strip(), n)
            for ngram in bigrams:
                elem = ''.join(list(ngram)) # concatenation of a tuple into a string
                if elem in n_grams_dictionary.keys():
                    line_array_bi.append(n_grams_dictionary.get(elem))
                else:
                    line_array_bi.append(n_grams_dictionary.get('<unk>'))
                    # bigram_arr - list of arrays, where elemet = array of bigram for a speciefic line
            bigram_arr.append(np.array(line_array_bi))
                    # final array of transformation input file bigrams into integers
        arr_uni = np.array(unigram_arr, dtype=object) # final array of arrays
        arr_bi = np.array(bigram_arr, dtype=object) # array of arrays
        print("done")
        seq_len = get_len_sentences('pad_len_' + file_name)
        original_seq_len = get_len_sentences(file_name)
        return arr_uni, arr_bi, arr, seq_len, original_seq_len
    
    
    
    
    
    
