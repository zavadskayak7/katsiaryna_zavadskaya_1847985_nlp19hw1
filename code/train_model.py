# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 19:28:40 2019

@author: zavad
"""
import tensorflow 
import keras as K
from keras.models import load_model
import numpy as np
import json

def run_model(arr_uni, arr_bi, arr, seq_len, org_seq_len, padding_len=70, train = True, resources_path = '.', output_path='out.txt'):
    # loading dictionary
    with open(resources_path + '/n_grams_dictionary_msr.json', 'r') as fp:
        n_grams_dictionary = json.load(fp)
    
    mask = np.zeros((arr_uni.shape[0], padding_len))
    for idx, l in enumerate(seq_len):
        mask[idx][:l] = np.ones(l)
    print(mask)
    
    if train:
        # PADDING
        # padding_len = 70 have it earlier in the code
        # creating inputs for NN, making every line of the arrays the same len = padding_len
        # and putting zeroes to the end of the line to fill not long enough lines
        train_x_uni = tensorflow.keras.preprocessing.sequence.pad_sequences(arr_uni, truncating='pre',\
                                                                        padding='post', maxlen=padding_len, value=0)
        train_x_bi = tensorflow.keras.preprocessing.sequence.pad_sequences(arr_bi, truncating='pre',\
                                                                           padding='post', maxlen=padding_len, value=0)
        train_y = tensorflow.keras.preprocessing.sequence.pad_sequences(arr, truncating='pre',\
                                                                        padding='post', maxlen=padding_len, value=0)
        train_y = tensorflow.keras.utils.to_categorical(train_y)
        
        #model
        # input layers for unigrams and bigrams
        # embedding layers also for both of them
        input_uni = tensorflow.keras.layers.Input(shape=(padding_len,))
        
        embedding_uni = tensorflow.keras.layers.Embedding(input_dim=len(n_grams_dictionary), output_dim = 300, \
                                                      mask_zero=True,input_length=padding_len)(input_uni)
        input_bi = tensorflow.keras.layers.Input(shape=(padding_len,))
        embedding_bi = tensorflow.keras.layers.Embedding(input_dim=len(n_grams_dictionary), output_dim = 300, \
                                                     mask_zero=True,input_length=padding_len)(input_bi)
        # concatenated embedding layers
        merged_uni_bi = tensorflow.keras.layers.concatenate([embedding_uni,embedding_bi])
        # bidirectional LSTM layer with dropout
        hidden_size = 256
        bi_lstm = tensorflow.keras.layers.Bidirectional(tensorflow.keras.layers.LSTM(hidden_size, dropout=0.3, \
                                                                                     recurrent_dropout=0.3, return_sequences=True), merge_mode='concat')(merged_uni_bi)
        # dense layer
        td_dense = tensorflow.keras.layers.TimeDistributed \
        (tensorflow.keras.layers.Dense(4, activation='softmax'))(bi_lstm)
        # SGD optimizer
        optimizer = tensorflow.keras.optimizers.SGD(lr=0.1, momentum=0.95, nesterov=True, clipnorm=1.)
        # determine model inputs and outputs
        model = tensorflow.keras.models.Model(inputs=[input_uni,input_bi], outputs=td_dense)
        # configuring the model for training
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, \
                      sample_weight_mode="temporal", metrics=None, \
                      weighted_metrics=['accuracy'])
    
        batch_size = 256
        epochs = 5
        saver = tensorflow.keras.callbacks.ModelCheckpoint(resources_path + '/K_model.h5', monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
        cbk = tensorflow.keras.callbacks.TensorBoard("logging/keras_model")
        print("\nStarting training...")
        model.fit(x=[train_x_uni,train_x_bi], y=train_y, epochs=epochs, batch_size=batch_size, \
              shuffle=True, callbacks=[cbk, saver], validation_split=0.2, sample_weight=mask) 
        print("Training complete.\n")
    else:
        model = tensorflow.keras.models.load_model(resources_path + '/K_model.h5')
        
        test_x_uni = tensorflow.keras.preprocessing.sequence.pad_sequences(arr_uni, truncating='pre',\
                                                                padding='post', maxlen=padding_len, value=0)
        test_x_bi = tensorflow.keras.preprocessing.sequence.pad_sequences(arr_bi, truncating='pre',\
                                                                padding='post', maxlen=padding_len, value=0)
        output_predictions = model.predict(x = [test_x_uni,test_x_bi], batch_size=32, verbose=1)
        
        pred_max =  np.argmax(output_predictions, axis=-1)
        
        final_pred = mask * pred_max
        
        bies_dict = {0:'B', 1:'I', 2:'E', 3:'S'}
        labeled_sentence = []
        for l,sentence in zip(seq_len,final_pred):
            for char in sentence[:l]:
                labeled_sentence.append(bies_dict.get(char))
            
        output_file = open(output_path , 'w')
        prev_len = 0
        for l in org_seq_len:
            pred_line = labeled_sentence[prev_len:(l+prev_len)]
            prev_len += l
            print(pred_line)
            predicted_int_line = ''.join(list(map(str,pred_line)))
            output_file.write(predicted_int_line)
            output_file.write('\n')
                    
        output_file.close()
